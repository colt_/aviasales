package main

import (
	"gitlab.com/aviasales/internal/app/anagram"
)

func main() {
	anagramService := &anagram.Anagram{}
	anagramService.Start()
}
