package anagram

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Anagram struct {
	Dictionary []string
	Word       string
}

func (a *Anagram) Start() {
	http.HandleFunc("/load", a.loadHandler)
	http.HandleFunc("/get", a.getHandler)

	server := &http.Server{Addr: ":" + "8080"}
	server.ListenAndServe()
}

func (a *Anagram) loadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		msg := "Method Not Allowed"
		fmt.Println(msg)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || string(body) == "" {
		fmt.Println("Bad Request")
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	d := strings.Replace(string(body), "[", "", -1)
	d = strings.Replace(d, "]", "", -1)
	d = strings.Replace(d, "\"", "", -1)
	d = strings.Replace(d, " ", "", -1)
	dictionary := strings.Split(d, ",")

	a.Dictionary = dictionary

	fmt.Println("Dictionary loaded")
	w.WriteHeader(http.StatusOK)
}

func (a *Anagram) getHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		msg := "Method Not Allowed"
		fmt.Println(msg)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}

	if len(a.Dictionary) == 0 {
		msg := "You need to download the dictionary"
		fmt.Println(msg)
		http.Error(w, msg, http.StatusFailedDependency)
		return
	}

	word, ok := r.URL.Query()["word"]
	if !ok || len(word[0]) < 1 {
		fmt.Println("Bad Request")
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	a.Word = strings.ToLower(string(word[0]))

	result := a.searchWordsByDictionary()

	w.WriteHeader(http.StatusOK)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(result)
}

func (a *Anagram) searchWordsByDictionary() []string {
	var words []string
	var matchedRune string
	needle := a.Word

	for _, str := range a.Dictionary {
		if len(str) != len(a.Word) {
			continue
		}

		for _, char := range str {
			rune := strings.ToLower(fmt.Sprintf("%c", char))

			for _, c := range needle {
				r := fmt.Sprintf("%c", c)

				if rune == r {
					matchedRune = matchedRune + rune
					needle = strings.Replace(needle, r, "", 1)
					break
				}
			}
		}

		if needle == "" {
			words = append(words, matchedRune)
		}

		needle = a.Word
		matchedRune = ""
	}

	return words
}
